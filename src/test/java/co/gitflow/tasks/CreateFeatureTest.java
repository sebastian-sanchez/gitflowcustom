package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.InitContext;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import static co.gitflow.GitFlowMultiBranchPlugin.DESCRIPTION;
import static co.gitflow.GitFlowMultiBranchPlugin.GRUPO;
import static org.junit.Assert.assertTrue;

public class CreateFeatureTest {


    InitContext context;
    Git git;
    CreateFeature createFeature;

    @Before
    public void setUp() throws Exception {

        //this.git = Git.open(new File("/Users/ssanchez/Documents/Proyectos/GitFlowProductions"));
        this.git = Git.open(new File("/Users/ssanchez/Documents/Proyectos/GitFlowProductions/testREpo"));

        this.context = new InitContext();
        //this.createFeature = new CreateFeature();
    }

    @Test
    public void generate() throws Exception {

        Project project = ProjectBuilder.builder().build();
        project.getPlugins().apply("co.git.multibranchs");

        Task tarea = project.getTasks().findByName("createFeature");

        tarea.setDescription(DESCRIPTION);
        tarea.setGroup(GRUPO);
        tarea.setProperty("context",context );
        tarea.setProperty("git", git);

        //this.createFeature = tarea;


        /*CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(),context.getPass());
        GitHelper.doFetchIfNeeded(git,credentialsProvider);
        GitHelper.checkoutBranch(git,this.context.getFeature().concat("MUMER_1020"),this.context.getDevelop());*/

    }





    @Test
    public void should_connect_to_public_repo() throws IOException, GitAPIException {
        final String REMOTE_URL = "https://github.com/lordofthejars/wildfly-example.git";

        // prepare a new folder for the cloned repository
        File localPath = File.createTempFile("TestGitRepository", "");
        localPath.delete();

        // then clone
        try (Git result = Git.cloneRepository()
                .setURI(REMOTE_URL)
                .setDirectory(localPath)
                .call()) {
            // Important to close the repo after being used
            System.out.println("Having repository: " + result.getRepository().getDirectory());
        }
    }


    @Test
    public void should_connect_using_user_pass() throws IOException, GitAPIException {
        final String REMOTE_URL = "https://asotobu@bitbucket.org/asotobu/backup.git";

        // prepare a new folder for the cloned repository
        File localPath = File.createTempFile("TestGitRepository", "");
        localPath.delete();

        // then clone
        try (Git result = Git.cloneRepository()
                .setURI(REMOTE_URL)
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider("username", "password"))
                .setDirectory(localPath)
                .call()) {

            System.out.println("Having repository: " + result.getRepository().getDirectory());
        }
    }

}