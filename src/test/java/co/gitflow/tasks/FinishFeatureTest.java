package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.InitContext;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class FinishFeatureTest {

    InitContext context;
    Git git;
    CreateFeature createFeature;

    @Before
    public void setUp() throws Exception {

        //this.git = Git.open(new File("/Users/ssanchez/Documents/Proyectos/GitFlowProductions"));
        this.git = Git.open(new File("/Users/ssanchez/Documents/Proyectos/GitFlowProductions/testREpo"));

        this.context = new InitContext();
        //this.createFeature = new CreateFeature(context,git);
    }

    @Test
    public void generate() throws Exception {
        String branchActual = GitHelper.currentBranch(git);
        CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(), context.getPass());
        GitHelper.doFetchIfNeeded(git, credentialsProvider);
        GitHelper.checkoutBranch(git, this.context.getDevelop());
        GitHelper.merege(git, branchActual);
        GitHelper.addTag(git, "pepe");
    }
}