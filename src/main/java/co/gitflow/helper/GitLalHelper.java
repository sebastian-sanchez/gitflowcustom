package co.gitflow.helper;

import co.gitflow.model.*;
import co.gitflow.util.GitlabHTTPRequestor;
import org.gradle.internal.impldep.com.fasterxml.jackson.databind.DeserializationFeature;
import org.gradle.internal.impldep.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import static co.gitflow.model.Method.DELETE;
import static co.gitflow.model.Method.POST;
import static co.gitflow.model.Method.PUT;

public class GitLalHelper {

    public static final ObjectMapper MAPPER = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private static final String API_NAMESPACE = "/api/v4";
    private static final String PARAM_SUDO = "sudo";
    private static final String PARAM_MAX_ITEMS_PER_PAGE = new Pagination().withPerPage(Pagination.MAX_ITEMS_PER_PAGE).toString();

    private final String hostUrl;

    private final String apiToken;
    private final TokenType tokenType;
    private AuthMethod authMethod;
    private boolean ignoreCertificateErrors = false;
    private Proxy proxy;
    private int defaultTimeout = 0;
    private int readTimeout = defaultTimeout;
    private int connectionTimeout = defaultTimeout;
    private String userAgent = GitLalHelper.class.getCanonicalName() + "/" + System.getProperty("java.version");


    private GitLalHelper(String hostUrl, String apiToken, TokenType tokenType, AuthMethod method) {
        this.hostUrl = hostUrl.endsWith("/") ? hostUrl.replaceAll("/$", "") : hostUrl;
        this.apiToken = apiToken;
        this.tokenType = tokenType;
        this.authMethod = method;
    }

    public static GitlabSession connect(String hostUrl, String username, String password) throws IOException {
        String tailUrl = GitlabSession.URL;
        GitLalHelper api = connect(hostUrl, null, null, null);
        return api.dispatch().with("login", username).with("password", password)
                .to(tailUrl, GitlabSession.class);
    }

    public static GitLalHelper connect(String hostUrl, String apiToken) {
        return new GitLalHelper(hostUrl, apiToken, TokenType.PRIVATE_TOKEN, AuthMethod.HEADER);
    }

    public static GitLalHelper connect(String hostUrl, String apiToken, TokenType tokenType) {
        return new GitLalHelper(hostUrl, apiToken, tokenType, AuthMethod.HEADER);
    }

    public static GitLalHelper connect(String hostUrl, String apiToken, TokenType tokenType, AuthMethod method) {
        return new GitLalHelper(hostUrl, apiToken, tokenType, method);
    }

    /**
     * Create a new MergeRequest
     *
     * @param projectId
     * @param sourceBranch
     * @param targetBranch
     * @param assigneeId
     * @param title
     * @return GitlabMergeRequest
     * @throws IOException on gitlab api call error
     */
    public GitlabMergeRequest createMergeRequest(Serializable projectId, String sourceBranch, String targetBranch,
                                                 Integer assigneeId, String title) throws IOException {

        Query query = new Query()
                .appendIf("target_branch", targetBranch)
                .appendIf("source_branch", sourceBranch)
                .appendIf("assignee_id", assigneeId)
                .appendIf("title", title);

        String tailUrl = GitlabProject.URL + "/" + sanitizeProjectId(projectId) + GitlabMergeRequest.URL + query.toString();

        return dispatch().to(tailUrl, GitlabMergeRequest.class);
    }


    /**
     * Updates a Merge Request
     *
     * @param projectId      The id of the project
     * @param mergeRequestId The id of the merge request to update
     * @param targetBranch   The target branch of the merge request, otherwise null to leave it untouched
     * @param assigneeId     The id of the assignee, otherwise null to leave it untouched
     * @param title          The title of the merge request, otherwise null to leave it untouched
     * @param description    The description of the merge request, otherwise null to leave it untouched
     * @param stateEvent     The state (close|reopen|merge) of the merge request, otherwise null to leave it untouched
     * @param labels         A comma separated list of labels, otherwise null to leave it untouched
     * @return the Merge Request
     * @throws IOException on gitlab api call error
     */
    public GitlabMergeRequest updateMergeRequest(Serializable projectId, Integer mergeRequestId, String targetBranch,
                                                 Integer assigneeId, String title, String description, String stateEvent,
                                                 String labels) throws IOException {
        Query query = new Query()
                .appendIf("target_branch", targetBranch)
                .appendIf("assignee_id", assigneeId)
                .appendIf("title", title)
                .appendIf("description", description)
                .appendIf("state_event", stateEvent)
                .appendIf("labels", labels);

        String tailUrl = GitlabProject.URL + "/" + sanitizeProjectId(projectId) + GitlabMergeRequest.URL + "/" + mergeRequestId + query.toString();

        return retrieve().method(PUT).to(tailUrl, GitlabMergeRequest.class);
    }




    /**
     * @param project            The Project
     * @param mergeRequestId     Merge Request ID
     * @param mergeCommitMessage optional merge commit message. Null if not set
     * @return new merge request status
     * @throws IOException on gitlab api call error
     */
    public GitlabMergeRequest acceptMergeRequest(GitlabProject project, Integer mergeRequestId, String mergeCommitMessage) throws IOException {
        return acceptMergeRequest(project.getId(), mergeRequestId, mergeCommitMessage);
    }

    public GitlabMergeRequest acceptMergeRequest(Serializable projectId, Integer mergeRequestId, String mergeCommitMessage) throws IOException {
        String tailUrl = GitlabProject.URL + "/" + sanitizeProjectId(projectId) + GitlabMergeRequest.URL + "/" + mergeRequestId + "/merge";
        GitlabHTTPRequestor requestor = retrieve().method(PUT);
        requestor.with("id", projectId);
        requestor.with("merge_request_id", mergeRequestId);
        if (mergeCommitMessage != null)
            requestor.with("merge_commit_message", mergeCommitMessage);
        return requestor.to(tailUrl, GitlabMergeRequest.class);
    }

/*

//    */
///**
//     * Update a Merge Request Note
//     *
//     * @param mergeRequest The merge request
//     * @param noteId       The id of the note
//     * @param body         The content of the note
//     * @return the Gitlab Note
//     * @throws IOException on gitlab api call error
//     *//*

//    public GitlabNote updateNote(GitlabMergeRequest mergeRequest, Integer noteId, String body) throws IOException {
//        Query query = new Query()
//                .appendIf("body", body);
//
//        String tailUrl = GitlabProject.URL + "/" + mergeRequest.getProjectId() +
//                GitlabMergeRequest.URL + "/" + mergeRequest.getIid() + GitlabNote.URL + "/" + noteId + query.toString();
//
//        return retrieve().method(PUT).to(tailUrl, GitlabNote.class);
//    }
//
//    public GitlabNote createNote(GitlabMergeRequest mergeRequest, String body) throws IOException {
//        String tailUrl = GitlabProject.URL + "/" + mergeRequest.getProjectId() +
//                GitlabMergeRequest.URL + "/" + mergeRequest.getIid() + GitlabNote.URL;
//
//        return dispatch().with("body", body).to(tailUrl, GitlabNote.class);
//    }
//
//    */
//   /*
//    /**
//     * Delete a Merge Request Note
//     *
//     * @param mergeRequest The merge request
//     * @param noteToDelete The note to delete
//     * @throws IOException on gitlab api call error
//     */
//
//    public void deleteNote(GitlabMergeRequest mergeRequest, GitlabNote noteToDelete) throws IOException {
//        String tailUrl = GitlabProject.URL + "/" + mergeRequest.getProjectId() + GitlabMergeRequest.URL + "/"
//                + mergeRequest.getIid() + GitlabNote.URL + "/" + noteToDelete.getId();
//        retrieve().method(DELETE).to(tailUrl, GitlabNote.class);
//    }
//*/



    private String sanitizeProjectId(Serializable projectId) {
        return sanitizeId(projectId, "projectId");
    }

    private String sanitizeGroupId(Serializable groupId) {
        return sanitizeId(groupId, "groupId");
    }

    private String sanitizeMilestoneId(Serializable milestoneId) {
        return sanitizeId(milestoneId, "milestoneId");
    }

    private String sanitizeId(Serializable id, String parameterName) {
        if (!(id instanceof String) && !(id instanceof Number)) {
            throw new IllegalArgumentException(parameterName + " needs to be of type String or Number");
        }

        try {
            return URLEncoder.encode(String.valueOf(id), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException((e));
        }
    }

    private String sanitizePath(String branch) {
        try {
            return URLEncoder.encode(branch, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException((e));
        }
    }


    public GitLalHelper setResponseReadTimeout(int readTimeout) {
        if (readTimeout < 0) {

            this.readTimeout = defaultTimeout;
        } else {
            this.readTimeout = readTimeout;
        }
        return this;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public GitLalHelper setConnectionTimeout(int connectionTimeout) {
        if (connectionTimeout < 0) {

            this.connectionTimeout = defaultTimeout;
        } else {
            this.connectionTimeout = connectionTimeout;
        }
        return this;
    }

    public GitlabHTTPRequestor retrieve() {
        return new GitlabHTTPRequestor(this).authenticate(apiToken, tokenType, authMethod);
    }

    public GitlabHTTPRequestor dispatch() {
        return new GitlabHTTPRequestor(this).authenticate(apiToken, tokenType, authMethod).method(POST);
    }

    public boolean isIgnoreCertificateErrors() {
        return ignoreCertificateErrors;
    }

    public Proxy getProxy() {
        return proxy;
    }

    public URL getAPIUrl(String tailAPIUrl) throws IOException {
        if (!tailAPIUrl.startsWith("/")) {
            tailAPIUrl = "/" + tailAPIUrl;
        }
        return new URL(hostUrl + API_NAMESPACE + tailAPIUrl);
    }

    public URL getUrl(String tailAPIUrl) throws IOException {
        if (!tailAPIUrl.startsWith("/")) {
            tailAPIUrl = "/" + tailAPIUrl;
        }

        return new URL(hostUrl + tailAPIUrl);
    }

    public String getHost() {
        return hostUrl;
    }

    public List<GitlabUser> getUsers() {
        String tailUrl = GitlabUser.URL + PARAM_MAX_ITEMS_PER_PAGE;
        return retrieve().getAll(tailUrl, GitlabUser[].class);
    }

    /**
     * Set the User-Agent header for the requests.
     *
     * @param userAgent
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public int getResponseReadTimeout() {
        return readTimeout;
    }

    /**
     * @deprecated use this.getResponseReadTimeout() method
     */
    @Deprecated
    public int getRequestTimeout() {
        return getResponseReadTimeout();
    }

}
