package co.gitflow.helper;


import org.gradle.internal.impldep.org.eclipse.jgit.api.*;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.TransportException;
import org.gradle.internal.impldep.org.eclipse.jgit.lib.Constants;
import org.gradle.internal.impldep.org.eclipse.jgit.lib.ObjectId;
import org.gradle.internal.impldep.org.eclipse.jgit.lib.Ref;
import org.gradle.internal.impldep.org.eclipse.jgit.lib.Repository;
import org.gradle.internal.impldep.org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class GitHelper {

    public static void doFetchIfNeeded(Git git, CredentialsProvider credentials) {
        try {
            System.out.println("1");
            git.fetch().setCheckFetchedObjects(true).call();
            System.out.println("2");

        } catch (TransportException e) {
            System.out.println("3");
            try {
                System.out.println("4 " + credentials);
                git.fetch()
                        .setCredentialsProvider(credentials)
                        .call();
            } catch (GitAPIException e1) {
                e1.printStackTrace();
            }
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }

    public static List<String> doListBranches(Git git) throws Exception {
        SortedSet<String> names = new TreeSet<String>();
        List<Ref> call = git.branchList().setListMode(ListBranchCommand.ListMode.ALL).call();
        for (Ref ref : call) {
            String name = ref.getName();
            int idx = name.lastIndexOf('/');
            if (idx >= 0) {
                name = name.substring(idx + 1);
            }
            if (name.length() > 0) {
                names.add(name);
            }
        }
        return new ArrayList<String>(names);
    }

    public static boolean localBranchExists(Git git, String branch) throws GitAPIException {
        List<Ref> list = git.branchList().call();
        String fullName = "refs/heads/" + branch;
        boolean localBranchExists = false;
        for (Ref ref : list) {
            String name = ref.getName();
            if (Objects.equals(name, fullName)) {
                localBranchExists = true;
            }
        }
        return localBranchExists;
    }

    public static String currentBranch(Git git) {
        try {
            return git.getRepository().getBranch();
        } catch (IOException e) {
            System.out.println("Failed to get the current branch due: " + e.getMessage() + ". This exception is ignored.");
            return null;
        }
    }


    public static void checkoutBranch(Git git, String branch, String origin) throws GitAPIException {
        String current = currentBranch(git);
        if (Objects.equals(current, branch)) {
            return;
        }
        System.out.println("Checking out branch: " + branch);
        // lets check if the branch exists
        CheckoutCommand command = git.checkout().setName(branch);
        boolean exists = localBranchExists(git, branch);
        if (!exists) {
            command = command.setCreateBranch(true).setForce(true).
                    setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).
                    setStartPoint(Constants.DEFAULT_REMOTE_NAME + "/" + origin);
        }

        command.call();

        //configureBranch(git, branch);
    }

    public static void checkoutBranch(Git git, String branch) throws Exception {
        String current = currentBranch(git);
        if (Objects.equals(current, branch)) {
            return;
        }
        System.out.println("Checking out branch: " + branch);
        // lets check if the branch exists
        CheckoutCommand command = git.checkout().setName(branch);
        boolean exists = localBranchExists(git, branch);
        if (!exists) {
            throw new Exception("No existe el brtanch");
        }

        command.call();

    }

    public static void addTag(Git git, String tagMessage) throws GitAPIException {
        Ref tag = git.tag().setName(tagMessage).call();
        System.out.println("Created/moved tag " + tag + " to repository at " + git.getRepository().getDirectory());
    }


    public static void merege(Git git,String branchMerege) throws GitAPIException, IOException {
        ObjectId mergeBase = git.getRepository().resolve(branchMerege);
        MergeResult merge = git.merge()
                .include(mergeBase)
                .setCommit(true)
                .setFastForward(MergeCommand.FastForwardMode.NO_FF)
                .setMessage("Merged changes").
                call();
        System.out.println("Merge-Results for id: " + mergeBase + ": " + merge);

      /*  for (Map.Entry<String,int[][]> entry : merge.getConflicts().entrySet()) {
            System.out.println("Key: " + entry.getKey());
            for(int[] arr : entry.getValue()) {
                System.out.println("value: " + Arrays.toString(arr));
            }
        }*/


    }

    public static Repository openJGitCookbookRepository() throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        return builder
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();
    }

    public static Repository openJGitCookbookRepository(File rootDirectory) throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.setGitDir(rootDirectory)
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();

        System.out.println("Having repository: " + repository.getDirectory());

        return repository;
    }

    public static Repository createNewRepository() throws IOException {
        // prepare a new folder
        File localPath = File.createTempFile("TestGitRepository", "");
        if (!localPath.delete()) {
            throw new IOException("Could not delete temporary file " + localPath);
        }

        // create the directory
        Repository repository = FileRepositoryBuilder.create(new File(localPath, ".git"));
        repository.create();

        return repository;
    }

}