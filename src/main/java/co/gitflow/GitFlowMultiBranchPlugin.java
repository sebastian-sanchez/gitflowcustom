package co.gitflow;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.InitContext;
import co.gitflow.tasks.CreateFeature;
import co.gitflow.tasks.FinishFeature;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.lib.Repository;

import java.io.File;
import java.io.IOException;

public class GitFlowMultiBranchPlugin implements Plugin<Project> {

    private Git initGit;
    private Repository repository;
    public static final String GRUPO = "RepocitoryTasks";
    public static final String DESCRIPTION = "Tareas para el manejo de repositorios mutual";

    @Override
    public void apply(final Project project) {

        System.out.print("start git flow mutibranchs ");

        InitContext initContext = project.getExtensions().create("initContext", InitContext.class);
        //initGit = getGitFromRoot(project.rootDir)
        repository = getGitRepository(project.getRootDir());

        initGit = new Git(repository);

        Task createFeture = project.getTasks().create("createFeature", CreateFeature.class);
        createFeture.setDescription(DESCRIPTION);
        createFeture.setGroup(GRUPO);
        createFeture.setProperty("context", initContext);
        createFeture.setProperty("git", initGit);
        createFeture.setProperty("nameBranch", project.getProperties().get("nameBranch"));



        Task finishFeature = project.getTasks().create("finishFeature", FinishFeature.class);
        finishFeature.setDescription(DESCRIPTION);
        finishFeature.setGroup(GRUPO);
        finishFeature.setProperty("context", initContext);
        finishFeature.setProperty("git", initGit);
        createFeture.setProperty("nameBranch", project.getProperties().get("nameBranch"));

      /*

        project.tasks.create('createBugfix', CreateFeature) {
            context = initContext
            git = initGit
            group GRUPO
            description DESCRIPTION

        }
        project.tasks.create('finishBugfix', CreateFeature) {
            context = initContext
            git = initGit
            group GRUPO
            description DESCRIPTION

        }
        project.tasks.create('createHotfix', CreateFeature) {
            context = initContext
            git = initGit
            group GRUPO
            description DESCRIPTION

        }
        project.tasks.create('finishHotfix', CreateFeature) {
            context = initContext
            git = initGit
            group GRUPO
            description DESCRIPTION
        }*/
    }


    private Repository getGitRepository(File rootDirectory) {
        try {
            return GitHelper.openJGitCookbookRepository();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
