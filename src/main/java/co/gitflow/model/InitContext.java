package co.gitflow.model;

public class InitContext {

    String master;
    String develop;
    String uat;
    String produccion;
    String feature;
    String bugfix;
    String hotfix;

    String user;
    String pass;

    public InitContext(String master,
                       String develop,
                       String uat,
                       String produccion,
                       String feature,
                       String bugfix,
                       String hotfix,
                       String user,
                       String pass) {
        this.master = master;
        this.develop = develop;
        this.uat = uat;
        this.produccion = produccion;
        this.feature = feature;
        this.bugfix = bugfix;
        this.hotfix = hotfix;
        this.user= user;
        this.pass = pass;
    }

    public InitContext() {
        this.master = "master";
        this.develop = "develop";
        this.uat = "UAT";
        this.produccion = "produccion";
        this.feature = "feature/";
        this.bugfix = "bugfix/";
        this.hotfix = "hotfix/";
        this.user= "sebastian-sanchez";
        this.pass = "msNLzBVTMHUJZ5zXh3BE";
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getDevelop() {
        return develop;
    }

    public void setDevelop(String develop) {
        this.develop = develop;
    }

    public String getUat() {
        return uat;
    }

    public void setUat(String uat) {
        this.uat = uat;
    }

    public String getProduccion() {
        return produccion;
    }

    public void setProduccion(String produccion) {
        this.produccion = produccion;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getBugfix() {
        return bugfix;
    }

    public void setBugfix(String bugfix) {
        this.bugfix = bugfix;
    }

    public String getHotfix() {
        return hotfix;
    }

    public void setHotfix(String hotfix) {
        this.hotfix = hotfix;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
