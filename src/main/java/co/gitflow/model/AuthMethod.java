package co.gitflow.model;

public enum AuthMethod {
    HEADER, URL_PARAMETER
}