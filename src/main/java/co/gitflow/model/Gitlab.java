package co.gitflow.model;

public class Gitlab {

    private String projectId;

    private String projectName;

    private String baseUri;

    private String apiUri = "/api/v3";

    public Gitlab(String projectId, String projectName, String baseUri, String apiUri) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.baseUri = baseUri;
        this.apiUri = apiUri;
    }
}
