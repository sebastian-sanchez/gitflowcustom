package co.gitflow.model;

public enum Method {

    GET, PUT, POST, PATCH, DELETE, HEAD, OPTIONS, TRACE;

}