package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.Gitlab;
import co.gitflow.model.InitContext;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class FinishBugFix extends DefaultTask {

    InitContext context;
    Git git;
    String nameBranchMerege;


    @TaskAction
    void generate() {

        String currentBranchName;
        try {
            currentBranchName = GitHelper.currentBranch(git);
            validateCurrentBranch(currentBranchName);
            CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(), context.getPass());
            GitHelper.doFetchIfNeeded(git, credentialsProvider);
            GitHelper.checkoutBranch(git, this.context.getDevelop());
            GitHelper.merege(git, currentBranchName);

            if (nameBranchMerege.equals(context.getMaster()) || nameBranchMerege.equals(context.getUat())) {
                GitHelper.checkoutBranch(git, this.context.getMaster());
                GitHelper.merege(git, currentBranchName);
            }
            if (nameBranchMerege.equals(context.getUat())) {
                GitHelper.checkoutBranch(git, this.context.getUat());
                GitHelper.merege(git, currentBranchName);
            }

            //TODO add r2 and release

            GitHelper.addTag(git, currentBranchName);
        } catch (GitAPIException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateCurrentBranch(String currentBranchName) throws Exception {
        if (!currentBranchName.startsWith(context.getBugfix())) {
            throw new Exception("The branch not is bugfix branch ".concat(currentBranchName));
        }
    }
}
