package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.helper.GitLalHelper;
import co.gitflow.model.GitlabUser;
import co.gitflow.model.InitContext;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.util.Objects;

public class FinishFeature extends DefaultTask {

    InitContext context;
    Git git;
    String nameBranch;

    private static GitLalHelper api;


    @TaskAction
    void generate() {
        String currentBranchName;
        try {
            currentBranchName = getNameBranch();

            validateCurrentBranch(currentBranchName);
            //CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(), context.getPass());
            //GitHelper.doFetchIfNeeded(git, credentialsProvider);
            //GitHelper.checkoutBranch(git, this.context.getDevelop());
            //GitHelper.merege(git, currentBranchName);
            GitHelper.addTag(git, currentBranchName);

            GitLalHelper api = GitLalHelper.connect("http://172.16.0.0:80", "test");
            api.setConnectionTimeout(100);
            api.createMergeRequest("",currentBranchName,this.context.getDevelop(),"","MERGE FEATURE");

        } catch (GitAPIException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getNameBranch() {
        String currentBranchName;
        if (Objects.isNull(nameBranch))
            currentBranchName = GitHelper.currentBranch(git);
        else
            currentBranchName = nameBranch;
        return currentBranchName;
    }

    private void validateCurrentBranch(String currentBranchName) throws Exception {
        if (!currentBranchName.startsWith(context.getFeature())) {
            throw new Exception("The branch not is feature branch ".concat(currentBranchName));
        }
    }
}
