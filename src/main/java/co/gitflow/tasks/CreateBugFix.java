package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.InitContext;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class CreateBugFix extends DefaultTask {

    InitContext context;
    Git git;
    String nameBranch ;


    @TaskAction
    void generate() {
        try {
            getLogger().lifecycle("Creating Merge Request For ${nameBranch}");
            System.out.print("-->" + nameBranch);
            String currentBranch = GitHelper.currentBranch(git);
            validateBugFix(currentBranch);
            CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(), context.getPass());
            GitHelper.doFetchIfNeeded(git, credentialsProvider);
            GitHelper.checkoutBranch(git, this.context.getBugfix().concat(nameBranch), currentBranch);
        } catch (GitAPIException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateBugFix(String nameBranch) throws Exception {
        if (context.getDevelop().equals(nameBranch) ||
                context.getProduccion().equals(nameBranch)) {
            throw new Exception("No se puede crear bugfix de produccion o develop");
        }


    }
}
