package co.gitflow.tasks;

import co.gitflow.helper.GitHelper;
import co.gitflow.model.InitContext;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.impldep.org.eclipse.jgit.api.Git;
import org.gradle.internal.impldep.org.eclipse.jgit.api.errors.GitAPIException;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.CredentialsProvider;
import org.gradle.internal.impldep.org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class CreateFeature extends DefaultTask {

    InitContext context;
    Git git;
    String nameBranch ;


    @TaskAction
    void generate() {
        try {

            CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(context.getUser(), context.getPass());
            GitHelper.doFetchIfNeeded(git, credentialsProvider);
            GitHelper.checkoutBranch(git, this.context.getFeature().concat(nameBranch), this.context.getDevelop());
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
    }
}
